---
title: Term 3
layout: page
---

{% for course in site.term3 %}
 [{{course.title}}]({{course.url|relative_url}})
{% endfor %}

<img style="max-width:65%" src='{{ "assets/images/Copy_of_Studio_-_3rd_Term.jpg" | relative_url }}' >

<img style="max-width:65%" src='{{ "assets/images/Copy_of_Studio_-_3rd_Term__1_.jpg" | relative_url }}'>
